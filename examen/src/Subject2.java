public class Subject2 {

    public static class Thread_model extends Thread {

        Thread_model(String name) {
            super(name);
        }

        public void run() {
            for (int i = 0; i < 8; i++) { // 8 messages per thread
                System.out.println(getName() + " - [" + System.currentTimeMillis() + "]");
                try {
                    sleep(1000);
                } catch (InterruptedException e) {
                    System.out.println("Error");
                }
            }
        }
    }

    public static void main(String[] args) {

        Thread_model NewThread1 = new Thread_model("NewThread1");
        Thread_model NewThread2 = new Thread_model("NewThread2");
        Thread_model NewThread3 = new Thread_model("NewThread3");
        Thread_model NewThread4 = new Thread_model("NewThread4");

        NewThread1.start();
        NewThread2.start();
        NewThread3.start();
        NewThread4.start();

    }
}
