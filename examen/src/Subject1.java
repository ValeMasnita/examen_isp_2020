public class Subject1 {

    public class X {

    }

    public class L {

        private int a;

        public void i(X x) {};

    }

    public class M {

        B b;
        A a;
        L l;

    }

    public class C {

    }

    public class A extends C {

        public void metA() {};

    }

    public class B {

        public void metB() {};

    }

}